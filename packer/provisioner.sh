#!/bin/sh

docker login -u "gitlab-ci-token" -p "$CI_JOB_TOKEN" "$CI_REGISTRY"

for image in $images; do
        echo "Pulling $CI_REGISTRY_IMAGE:$image-${IMAGE_TAG}"
        docker pull "$CI_REGISTRY_IMAGE:$image-${IMAGE_TAG}"
done

docker logout "$CI_REGISTRY"

# Disable automatic CoreOS updates
echo "Disabling automatic updates"
sudo systemctl mask update-engine.service
sudo systemctl mask locksmithd.service

#!/bin/sh

# Disable automatic CoreOS updates
echo "Disabling automatic updates"
sudo systemctl mask update-engine.service
sudo systemctl mask locksmithd.service

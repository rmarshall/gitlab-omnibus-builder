# SuSE Enterprise Linux (SLES)

## Overview
GitLab currently packages for SLES version(s): 12 SP2

To package for these versions, we need to make use of SLES based containers that
are either on a dedicated SLES host (isolated, prone to lack of care), or containers
made from a SLES host, and pointed to a [Subscription Management Tool (SMT)][SMT]
server as a providing mirror.

Due to licensing concerns, GitLab does not provide the associated SLES-based
containers on the public registry for GitLab Omnibus Builder.

## Containers

The containers are provided on GitLab's Dev instance only. They are created
during the CI process, and will only run on the Dev instance. They use a [Dockerfile][] 
that includes all repository additions, package installation, and repository
removals in a single step, so that we maintain the privacy of the `SMT_HOST`.
`SMT_HOST` is provided as a part of the CI secrets in the project.


## About the SMT

Sensitive information regarding the SMT server can be found in the `SLES SMT` note
of the `Build` vault.

The SMT server acts as a local package mirror for the containers to be able to
install packages that are up to date for the versions of SLES currently being
supported. The estimated storage requirements are 10 GB per version.

Managment of the available mirrored repositories is handled via [YaST][]. You 
will find the mirroring configuration under `Network Services > SMT Server Management`.
Documentation about how this section is configured is provided [here](https://www.suse.com/documentation/sles-12/book_smt/data/smt_yast_staging_starting.html)

Because this is a licensed instance, we can not expose it publicly.



[SMT]: https://www.suse.com/documentation/sles-12/book_smt/data/book_smt.html
[Dockerfile]: https://docs.docker.com/engine/reference/builder/
[YaST]: https://yast.github.io/